package ru.t1.amsmirnov.taskmanager.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.ITaskService;
import ru.t1.amsmirnov.taskmanager.comparator.NameComparator;
import ru.t1.amsmirnov.taskmanager.comparator.StatusComparator;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.TaskNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.NameEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.ProjectIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;

import java.util.*;

public final class TaskService implements ITaskService {

    @NotNull
    private final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public TaskDTO add(@Nullable final TaskDTO task) throws AbstractException {
        if (task == null) throw new TaskNotFoundException();
        final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(task);
            sqlSession.commit();
        } catch (final Exception exception) {
            sqlSession.rollback();
            throw exception;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    public TaskDTO add(
            @Nullable final String userId,
            @Nullable final TaskDTO task
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Collection<TaskDTO> addAll(@Nullable final Collection<TaskDTO> tasks) throws AbstractException {
        if (tasks == null) throw new ModelNotFoundException(TaskDTO.class.getName());
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            for (TaskDTO task : tasks) repository.add(task);
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return tasks;
    }

    @NotNull
    @Override
    public Collection<TaskDTO> set(@Nullable final Collection<TaskDTO> tasks) throws AbstractException {
        if (tasks == null) throw new ModelNotFoundException(TaskDTO.class.getName());
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            repository.removeAll();
            for (TaskDTO task : tasks) repository.add(task);
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() throws AbstractException {
        @Nullable final List<TaskDTO> tasks;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            tasks = repository.findAll();
            if (tasks == null)
                return Collections.emptyList();
        }
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final Comparator<TaskDTO> comparator) throws AbstractException {
        @Nullable final List<TaskDTO> tasks;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            tasks = repository.findAllSorted(getComparator(comparator));
            if (tasks == null)
                return Collections.emptyList();
        }
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<TaskDTO> tasks;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            tasks = repository.findAllUserId(userId);
            if (tasks == null)
                return Collections.emptyList();
        }
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<TaskDTO> comparator
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<TaskDTO> tasks;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            tasks = repository.findAllSortedUserId(userId, getComparator(comparator));
            if (tasks == null)
                return Collections.emptyList();
        }
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        try (@NotNull final SqlSession connection = getConnection()) {
            @NotNull final ITaskRepository taskRepository = connection.getMapper(ITaskRepository.class);
            final List<TaskDTO> tasks = taskRepository.findAllByProjectId(userId, projectId);
            if (tasks == null) return new ArrayList<>();
            return tasks;
        }
    }

    @NotNull
    @Override
    public TaskDTO findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneById(id);
        }
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public TaskDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneByIdUserId(userId, id);
        }
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public TaskDTO update(@Nullable final TaskDTO task) throws AbstractException {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }


    @NotNull
    @Override
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskDTO task = findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        return update(task);
    }

    @NotNull
    @Override
    public TaskDTO changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final TaskDTO task = findOneById(userId, id);
        if (status == null) return task;
        task.setStatus(status);
        return update(task);
    }

    @Override
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws AbstractException {
        if (userId == null) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeAllUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final Collection<TaskDTO> collection) {
        if (collection == null) return;
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            for (final TaskDTO task : collection)
                repository.removeOneByIdUserId(task.getUserId(), task.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO removeOne(@Nullable final TaskDTO task) throws AbstractException {
        if (task == null) throw new TaskNotFoundException();
        return removeOneById(task.getId());
    }

    @NotNull
    @Override
    public TaskDTO removeOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        final TaskDTO task;
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneById(id);
            repository.removeOneById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @NotNull
    @Override
    public TaskDTO removeOne(
            @Nullable final String userId,
            @Nullable final TaskDTO task
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new ModelNotFoundException("TaskDTO");
        return removeOneById(userId, task.getId());
    }

    @NotNull
    @Override
    public TaskDTO removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final TaskDTO task;
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneById(id);
            repository.removeOneByIdUserId(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public int getSize() {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.getSize();
        }
    }

    @Override
    public int getSize(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.getSizeUserId(userId);
        }
    }

    @Override
    public boolean existById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.existById(id);
        }
    }

    @Override
    public boolean existById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.existByIdUserId(userId, id);
        }
    }

    private String getComparator(Comparator<?> comparator) {
        if (comparator instanceof StatusComparator) return "status";
        if (comparator instanceof NameComparator) return "name";
        return "created";
    }

    @NotNull
    public SqlSession getConnection() {
        return connectionService.getConnection();
    }

}
