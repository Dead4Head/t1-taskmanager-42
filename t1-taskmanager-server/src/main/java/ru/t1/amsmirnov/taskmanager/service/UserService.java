package ru.t1.amsmirnov.taskmanager.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.IUserRepository;
import ru.t1.amsmirnov.taskmanager.api.service.*;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.UserNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.EmailEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.LoginEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.PasswordEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.user.EmailExistException;
import ru.t1.amsmirnov.taskmanager.exception.user.LoginExistException;
import ru.t1.amsmirnov.taskmanager.util.HashUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public final class UserService implements IUserService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService,
            @NotNull final IPropertyService propertyService
    ) {
        this.connectionService = connectionService;
        this.projectService = projectService;
        this.taskService = taskService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmailExist(email)) throw new EmailExistException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        if (role != null) user.setRole(role);
        return add(user);
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException {
        return create(login, password, email, null);
    }

    @NotNull
    @Override
    public UserDTO add(@Nullable final UserDTO user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.add(user);
            sqlSession.commit();
        } catch (final Exception exception) {
            sqlSession.rollback();
            throw exception;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    public Collection<UserDTO> addAll(@Nullable final Collection<UserDTO> users) throws AbstractException {
        if (users == null) throw new ModelNotFoundException(UserDTO.class.getName());
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            for (UserDTO user : users) repository.add(user);
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return users;
    }

    @NotNull
    @Override
    public Collection<UserDTO> set(@Nullable final Collection<UserDTO> users) throws AbstractException {
        if (users == null) throw new ModelNotFoundException(UserDTO.class.getName());
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
        try {
            repository.removeAll();
            for (UserDTO user : users) repository.add(user);
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return users;
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() throws AbstractException {
        @Nullable final List<UserDTO> users;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            users = repository.findAll();
            if (users == null)
                return Collections.emptyList();
        }
        return users;
    }

    @NotNull
    @Override
    public List<UserDTO> findAll(@Nullable final Comparator<UserDTO> comparator) throws AbstractException {
        return findAll();
    }

    @NotNull
    @Override
    public UserDTO findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            user = repository.findOneById(id);
        }
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public UserDTO findOneByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
            @Nullable final UserDTO user = userRepository.findOneByLogin(login);
            if (user == null) throw new UserNotFoundException();
            return user;
        }
    }

    @NotNull
    @Override
    public UserDTO findOneByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
            @Nullable final UserDTO user = userRepository.findOneByEmail(email);
            if (user == null) throw new UserNotFoundException();
            return user;
        }
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
            return userRepository.isLoginExist(login);
        }
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = connection.getMapper(IUserRepository.class);
            return userRepository.isEmailExist(email);
        }
    }

    @NotNull
    @Override
    public UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = findOneById(id);
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        return update(user);
    }

    @NotNull
    @Override
    public UserDTO update(@Nullable final UserDTO user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.update(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }


    @NotNull
    @Override
    public UserDTO updateUserById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws AbstractException {
        @NotNull UserDTO user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return update(user);
    }

    @Override
    public void lockUserByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTO user = findOneByLogin(login);
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final UserDTO user = findOneByLogin(login);
        user.setLocked(false);
        update(user);
    }

    @NotNull
    @Override
    public UserDTO removeOne(@Nullable final UserDTO user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            taskService.removeAll(userId);
            projectService.removeAll(userId);
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.removeOneById(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO removeByLogin(@Nullable final String login) throws AbstractException {
        @NotNull final UserDTO user = findOneByLogin(login);
        return removeOne(user);
    }

    @NotNull
    @Override
    public UserDTO removeByEmail(@Nullable final String email) throws AbstractException {
        @NotNull final UserDTO user = findOneByEmail(email);
        return removeOne(user);
    }


    @Override
    // TODO: 09.08.2022 task and project clear
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            taskService.removeAll();
            projectService.removeAll();
            repository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final Collection<UserDTO> collection) {
        if (collection == null) return;
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            for (final UserDTO user : collection)
                repository.removeOneById(user.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public UserDTO removeOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            taskService.removeAll(id);
            projectService.removeAll(id);
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.removeOneById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return new UserDTO();
    }

    @Override
    public int getSize() {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.getSize();
        }
    }

    @Override
    public boolean existById(@NotNull String id) throws AbstractException {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.existById(id);
        }
    }
}
