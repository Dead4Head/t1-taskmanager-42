package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public interface IUserService extends IService<UserDTO> {

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws AbstractException;

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email,
            @Nullable Role role
    ) throws AbstractException;

    @NotNull
    UserDTO removeByLogin(@Nullable String login) throws AbstractException;

    @NotNull
    UserDTO removeByEmail(@Nullable String email) throws AbstractException;

    @NotNull
    UserDTO setPassword(
            @Nullable String id,
            @Nullable String password
    ) throws AbstractException;

    @NotNull
    UserDTO updateUserById(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws AbstractException;

    void lockUserByLogin(@Nullable String login) throws AbstractException;

    void unlockUserByLogin(@Nullable String login) throws AbstractException;

    @NotNull
    UserDTO findOneByLogin(@Nullable String login) throws AbstractException;

    @NotNull
    UserDTO findOneByEmail(@Nullable String email) throws AbstractException;

    boolean isLoginExist(@Nullable String login) throws AbstractException;

    boolean isEmailExist(@Nullable String email) throws AbstractException;

}
