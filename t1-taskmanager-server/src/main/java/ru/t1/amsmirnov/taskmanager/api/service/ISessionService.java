package ru.t1.amsmirnov.taskmanager.api.service;

import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;

public interface ISessionService extends IUserOwnedService<SessionDTO> {
}
