package ru.t1.amsmirnov.taskmanager.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    SqlSession getConnection();

    @NotNull
    SqlSessionFactory getSqlSessionFactory();

    @NotNull
    EntityManagerFactory getEntityManagerFactory();

}
