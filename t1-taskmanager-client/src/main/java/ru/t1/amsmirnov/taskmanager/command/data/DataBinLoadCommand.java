package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataBinLoadRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataBinLoadResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class DataBinLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    public static final String DESCRIPTION = "Load data from binary dump file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA BINARY LOAD]");
        @NotNull final DataBinLoadRequest request = new DataBinLoadRequest(getToken());
        @NotNull final DataBinLoadResponse response =  getDomainEndpoint().loadDataBin(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
