package ru.t1.amsmirnov.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLoginRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLogoutRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserProfileRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserLoginResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserLogoutResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.net.MalformedURLException;

@WebService
public interface IAuthEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance() {
        return IEndpoint.newInstance(NAME, PART, IAuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserLoginResponse login(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserLoginRequest request);

    @NotNull
    @WebMethod
    UserLogoutResponse logout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserLogoutRequest request);

}
